from .action import Action2
from mud.events import StartEvent,StartedEvent


class StartedAction(Action2):
    EVENT = StartedEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "started"


class StartAction(Action2):
    EVENT = StartEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "start"