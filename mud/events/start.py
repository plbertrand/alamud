# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class StartedEvent(Event2):
    NAME ="started"

    def perform(self):
        if not self.object.has_prop("startable"):
            self.add("object-not-startable")
            self.fail()
            return self.inform("started.failed")
        self.inform("started")


class StartEvent(Event2):
    NAME = "start"

    def perform(self):
        if not self.object.has_prop("startable"):
            self.add_prop("object-not-startable")
            return self.take_failed()
        self.inform("start")

    def start_failed(self):
        self.fail()
        self.inform("start.failed")
